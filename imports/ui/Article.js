import React from 'react';

export default class Article extends React.Component {
  render() {
    let itemClassName = `item item--position-${this.props.article.rank}`;

    return (
      <div key={this.props.article._id} className={itemClassName}>
        <div className="article">
          <div>
            <h3 className="article__name">{this.props.article.name}</h3>
            <p className="article__stats">
              {this.props.article.position} place - {this.props.article.score} point(s).
            </p>
          </div>
          <div className="article__actions">
            
            <button className="button button--round" onClick={() => {
              console.log("click buttom");
              //Players.update(this.props.article._id, {$inc: {score: -1}});
            }}>-1</button>

            <button className="button button--round" onClick={() => {
               console.log("click buttom");
                // Players.update(this.props.player._id, {$inc: {score: 1}});
            }}>+1</button>
            <button className="button button--round" onClick={() => { 
            //  Players.remove(this.props.player._id)
               console.log("click buttom");
            }}>X</button>
          </div>
        </div>
      </div>
    );
  }
};

// Setup prop types. article should be a required object
Article.propTypes = {
  article: React.PropTypes.object.isRequired
};
