import React from 'react';

import TitleBar from './TitleBar';
import AddArticle from './AddArticle';
import ArticleList from './ArticleList';

export default class App extends React.Component {
  render() {
    return (
      <div>
        <TitleBar title={this.props.title} subtitle="The best way to trip"/>
        <div className="wrapper">
          <ArticleList articles={this.props.articles}/>
          <AddArticle />
        </div>
      </div>
    );
  }
};

App.propTypes = {
  title: React.PropTypes.string.isRequired,
  articles: React.PropTypes.array.isRequired
};
