import React from 'react';

//import {articles} from '../api/articles';

export default class AddArticle extends React.Component {
  handleSubmit(e) {
    let articleName = e.target.articleName.value;

    e.preventDefault();

    if (articleName) {
      e.target.articleName.value = '';
      //add to database
    }
  }
  render() {
    return (
      <div className="item">
        <form className="form" onSubmit={this.handleSubmit.bind(this)}>
          <input className="form__input" type="text" name="articleName" placeholder="article name"/>
          <button className="button">Add article</button>
        </form>
      </div>
    );
  }
};
