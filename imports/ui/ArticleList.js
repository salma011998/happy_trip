import React from 'react';
import FlipMove from 'react-flip-move';

import Article from './Article';

export default class ArticleList extends React.Component {
  renderArticles() {
    if (this.props.articles.length === 0) {
      return (
        <div className="item">
          <p className="item__message">Add your first advice to get started!</p>
        </div>
      );
    } else {
      return this.props.articles.map((article) => {
        return <Article key={article._id} article={article}/>;
      });
    }
  }
  render() {
    return (
      <div>
        <FlipMove maintainContainerHeight={true}>
          {this.renderArticles()}
        </FlipMove>
      </div>
    );
  }
};

ArticleList.propTypes = {
  articles: React.PropTypes.array.isRequired
}
